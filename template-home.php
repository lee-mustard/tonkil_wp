<?php
/**
 * Template Name: Home Page
 */
?>

<?php while (have_posts()) : the_post(); ?>

<!-- <h1 class="word_break">G</h1>
<h1 class="word_break_child">IVE</h1> -->
<section id="intro">
    <div class="container">

      <img class="mx-auto img-fluid" src="<?php bloginfo('template_directory'); ?>/assets/images/header_img.jpg" />

      <div class="cotent_holder">
        <div class="row align-items-center"> 
              <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 intro_content">
                  <p>Tonkil Abramson is the boutique optometrist that delivers world class eye care with personalised service. </p>
                  <p>Mark Tonkil has over two decades of experience across all aspects of optometry and offers a full scope of services to assess and design a solution tailored to each individual.  His family run practice uses cutting edge technology paired with genuine care, and they take pride in sourcing and advising on the latest eyewear and eye care products, to ensure that each client finds their perfect fit.</p>
              </div>  
              
              <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 negative_m">
                <div style="display: inline-block; text-align: left;">
                  <h1 clas="word_break title_sperator_1">
                        Eyecare <br />
                        with <br/> 
                        Flair <br />
                    </h1>
                </div>
              </div>  
          </div>

      </div>
    </div>
</section>
 
<section id="services">
    <div class="container">
        <div class="row align-items-center">


          <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12" data-aos="fade-up">
              <h1 clas="word_break title_sperator_1">Our <br />Services </h1>
               <hr class="sm-hr"> 
          </div>

          <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 bg_holder" data-aos="fade-up">
            <div class="services_holder">
              <p>Eye examinations</p>
              <p>Prescription spectacles </p>
              <p>All contact lenses</p>
              <p>Glaucoma screenings</p>
              <p>Free drivers screenings</p>
              <p>Visual screenings</p>
              <p>Ready readers</p>
              <p>Sunglasses</p>
              <p>Frame repairs</p>
			  <p>Optical accessories</p>
            </div>
          </div>

      </div>
  </div>
</section>


<section id="contact">
    <div class="container">
      <div class="row align-items-center">
       
          <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12"  data-aos="fade-left">
            <h1 clas="word_break"><span>Get in<br />Touch</span></h1>
            
          </div>
    
          <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12"  data-aos="fade-right">
              <?php gravity_form(1, false, false, false, '', true, 12); ?>
          </div>

      </div>
  </div>
</section>


<section id="map-contact">
    <div class="container">
      <div class="row align-items-center">
       
          <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12" data-aos="zoom-in-down">
            <div class="content_hold center_mobile">
                <p><i style="margin-right:5px;" class="fas fa-phone-alt"></i> <a href="tel:0214399439 ">+27 21 439 9439</a> / <a href="tel:0214395472 ">+27 21 439 5472</a><br /><i  style="margin-right:5px;" class="far fa-envelope"></i> <a href="mailto:seapoint@tonkiloptometrist.co.za">seapoint@tonkiloptometrist.co.za</a></p> <br />
                <p>SHOP 12, THE POINT MALL, <br />76 REGENT RD, SEA POINT<br />CAPE TOWN, 8005</p>
                <br />
                <a target="_blank" class="icon_link" href="https://www.facebook.com/Tonkil.Optometrists/"><i class="fab fa-facebook"></i></a> <a target="_blank" class="icon_link"  href="https://www.linkedin.com/in/mark-tonkil-373b7b23"><i class="fab fa-linkedin"></i></a> <a target="_blank" class="icon_link"  href="https://instagram.com/tonkilabramson?igshid=pjv4mb64558s"><i class="fab fa-instagram"></i></a>
            
            </div>  
          </div>
    
          <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12" data-aos="zoom-in-left">
              <a target="_blank" href="https://goo.gl/maps/6ip9AKJPmBhnYm5G8"><div id="map"></div></a>


              <script>
                function initMap() {
                  var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 16,
                    center: {lat: -33.9206256, lng: 18.3833901}, 
                    disableDefaultUI: false,
                  });

                  var myLatLng = {lat: -33.920598, lng: 18.383176};

                  var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 17,
                    center: myLatLng,
                    disableDefaultUI: true,
                    styles:     [{"featureType":"administrative","elementType":"all","stylers":[{"saturation":"-100"}]},{"featureType":"administrative.province","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"all","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","elementType":"all","stylers":[{"saturation":-100},{"lightness":"50"},{"visibility":"simplified"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":"-100"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"all","stylers":[{"lightness":"30"}]},{"featureType":"road.local","elementType":"all","stylers":[{"lightness":"40"}]},{"featureType":"transit","elementType":"all","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-15},{"saturation":-97}]},{"featureType":"water","elementType":"labels","stylers":[{"lightness":-15},{"saturation":-100}]}],

                  });

                  var marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map,
                    disableDefaultUI: false,
                    title: 'Hello World!',
                  });
                }
              </script>

          </div>

      </div>
  </div>
</section>

<?php endwhile; ?>