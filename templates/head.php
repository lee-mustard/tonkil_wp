<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="icon" type="image/png" href="<?php bloginfo('template_directory'); ?>/assets/images/favicon-32x32.png" sizes="32x32" />
  <link rel="icon" type="image/png" href="<?php bloginfo('template_directory'); ?>/assets/images/favicon-16x16.png" sizes="16x16" />

<!-- COMMON TAGS -->
<meta charset="utf-8">
<title>Tonkil Abramson Optometrists</title>
<!-- Search Engine -->
<meta name="description" content="Eyecare With Flair ">
<meta name="image" content="https://tonkiloptometrist.co.za/wp-content/themes/tonkil/assets/images/header_img.jpg">
<!-- Schema.org for Google -->
<meta itemprop="name" content="Tonkil Abramson Optometrists">
<meta itemprop="description" content="Eyecare With Flair ">
<meta itemprop="image" content="https://tonkiloptometrist.co.za/wp-content/themes/tonkil/assets/images/header_img.jpg">
<!-- Open Graph general (Facebook, Pinterest & Google+) -->
<meta name="og:title" content="Tonkil Abramson Optometrists">
<meta name="og:description" content="Eyecare With Flair ">
<meta name="og:image" content="https://tonkiloptometrist.co.za/wp-content/themes/tonkil/assets/images/header_img.jpg">
<meta name="og:url" content="https://tonkiloptometrist.co.za/">
<meta name="og:site_name" content="Tonkil Abramson Optometrists">
<meta name="og:type" content="website">

  <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />

  <?php wp_head(); ?>
</head>
