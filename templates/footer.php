<footer class="content-info">
  <div class="container">

    <div class="d-flex justify-content-between">
        <div class="l_foot">
          &copy; Tonkil Abramson Optometrists.
        </div>
        <div class="r_foot">
          Design by <a target="_blank" href="https://mustard.agency">Mustard</a>
        </div>
    </div>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBo2jZ5InmhTiTS3mnQKRRylu518nWjplU&callback=initMap"> </script>
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
  <script>
    AOS.init();
  </script>
  </div>
</footer>