<header class="banner">
  <div class="container">
    <nav class="nav-primary">
      <?php
      if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
      endif;
      ?>
    </nav>
  </div>
</header>

<div class="border-t container">
  <a href="<?= esc_url(home_url('/')); ?>">
      <img class="mx-auto d-block logo-header" src="<?php bloginfo('template_directory'); ?>/assets/images/logo.png" />
  </a>
</div>